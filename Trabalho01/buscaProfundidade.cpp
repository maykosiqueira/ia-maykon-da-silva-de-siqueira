
#include "buscaProfundidade.h"

BuscaProfundidade::BuscaProfundidade(Hanoi h) {
    estados.push_back(h);
    contador = 0;
}

//Realiza o processamento da torre de hanoi
void BuscaProfundidade::processar() {
    list<Hanoi> aux;
    vector<Hanoi> todos;
    vector<int> resultado;
    int percorridos=0;

    Hanoi tmp;
    processados.push_back(estados.front());
    todos.push_back(estados.front());

    while(true) {
        tmp = estados.front();
        if(tmp.fimAlcancado()) { //Verifica se alcancou o fim
            todos.push_back(tmp);
            resultado.push_back(todos.size()-1);

            //Pega as ordens dos pais
            while(tmp.getPai() != 0){
                resultado.push_back(tmp.getPai());
                tmp = todos[tmp.getPai()];
            }

            //Inverte para sair corretamene
            reverse(resultado.begin(), resultado.end());
            percorridos = resultado.size();
            for(int i=0;i<percorridos;i++){
                todos[resultado[i]].exibePinos();
            }

            //Percorridos-1 devido a quantidade de nohs percorridos ignorar o inicial
            cout << endl << endl << "Quantidade de nohs percorridos: " << percorridos-1 << endl;
            cout << "Quantidade de nohs conferidos: " << contador << endl;
            return;
            return;
        } else {
            contador++;
            if(tmp.mover(0,1)) {
                if(!jahProcessado(tmp)) {
                    //Seta o pai
                    tmp.setPai(contador);
                    processados.push_back(tmp);
                    aux.push_front(tmp);//Joga para auxiliar e depois traz para os estados para fazer a busca em profundidade
                }
            }
            tmp = estados.front();
            if(tmp.mover(0,2) ) {
                if(!jahProcessado(tmp)) {
                    //Seta o pai
                    tmp.setPai(contador);
                    processados.push_back(tmp);
                    aux.push_front(tmp);
                }
            }

            tmp = estados.front();
            if(tmp.mover(1,2)) {
                if(!jahProcessado(tmp)) {
                    //Seta o pai
                    tmp.setPai(contador);
                    processados.push_back(tmp);
                    aux.push_front(tmp);
                }
            }

            tmp = estados.front();
            if(tmp.mover(2,0)) {
                if(!jahProcessado(tmp)) {
                    //Seta o pai
                    tmp.setPai(contador);
                    processados.push_back(tmp);
                    aux.push_front(tmp);
                }
            }

            tmp = estados.front();
            if(tmp.mover(1,0)) {
                if(!jahProcessado(tmp)) {
                    //Seta o pai
                    tmp.setPai(contador);
                    processados.push_back(tmp);
                    aux.push_front(tmp);
                }
            }

            tmp = estados.front();
            if(tmp.mover(2,1) ) {
                if(!jahProcessado(tmp)) {
                    //Seta o pai
                    tmp.setPai(contador);
                    processados.push_back(tmp);
                    aux.push_front(tmp);
                }
            }
            todos.push_back(estados.front());
            estados.pop_front();

            //Remove tudo o que tinha ido para o auxiliar e volta para o estado
            //É colocado os estados encontrados no auxiliar para poder processar e entao apagar
            //o Noh que acabou de ser processado
            //Ai une a lista aux com a de estados para fazera  busca em profundidade ao inves de largura
            while(!aux.empty()){
                estados.push_front(aux.back());
                aux.pop_back();
            }
        }
    }
}
bool BuscaProfundidade::jahProcessado(Hanoi h) {
    for(int i=0; i<processados.size(); i++) {
        if(h.estadoIgual(processados[i]))
            return true;
    }
    return false;
}
