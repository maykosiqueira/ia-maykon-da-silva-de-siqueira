#include "Hanoi.h"

Hanoi::Hanoi(){
    //Aloca os vetores
    for(int i=0;i<3;i++){
        vector<int> rows;
        pinos.push_back(rows);
    }

    pai = 0;
}
Hanoi::Hanoi(int disco){

    //Aloca os vetores
    for(int i=0;i<3;i++){
        vector<int> rows;
        pinos.push_back(rows);
    }

    //Preenche o disco
    for(int i = disco;i>0;i--){
        pinos[0].push_back(i);
    }

    pai = 0;
}
bool Hanoi::mover(int pinoOrigem, int pinoDestino){
    //Checa se os pinos especificados sao validos
    if(pinoOrigem < 0 || pinoOrigem > 2 || pinoDestino < 0 || pinoDestino > 2 || pinoOrigem == pinoDestino){
        return false;
    }
    int topPinoOrigem;
    int topPinoDestino;

    //Pega os valores que estao no topo para validar a informacao
    if(pinos[pinoOrigem].empty()){
        return false;
    }
    topPinoOrigem = pinos[pinoOrigem].back();
    if(pinos[pinoDestino].empty()){
        topPinoDestino = topPinoOrigem + 1;
    }else{
        topPinoDestino = pinos[pinoDestino].back();
    }


    //Ve se o que estava no topo eh menor na origem do que destino
    //Porque se nao for a jogada eh invalida
    if(topPinoOrigem>topPinoDestino){
        return false;
    }

    //Se for valido,realiza o movimento
    pinos[pinoDestino].push_back(topPinoOrigem);
    pinos[pinoOrigem].pop_back();

    return true;
}

void Hanoi::exibePinos(){
    int p1Size = pinos[0].size();
    int p2Size = pinos[1].size();
    int p3Size = pinos[2].size();
    int maior;

    //Pega o maior elemento
    if(p1Size > p2Size){
        maior = p1Size;
    }else{
        maior = p2Size;
    }

    if(p3Size > maior){
        maior = p3Size;
    }

    cout << endl << "------------------------------" << endl;
    for(int i=maior;i>0;i--){
        if(p1Size >= i){
            cout << centerOut(10,to_string(pinos[0][i-1]));
        }else{
            cout << centerOut(10,"| ");
        }
        if(p2Size >= i){
            cout << centerOut(10,to_string(pinos[1][i-1]));
        }else{
            cout << centerOut(10,"|");
        }
        if(p3Size >= i){
            cout << centerOut(10,to_string(pinos[2][i-1]));
        }else{
            cout << centerOut(10,"|");
        }
        cout << endl;
    }

    cout << centerOut(10,"========");
    cout << centerOut(10,"========");
    cout << centerOut(10,"========");
    cout << endl << "------------------------------" ;

}
bool Hanoi::fimAlcancado(){
    if(!pinos[0].empty() ||!pinos[1].empty() ){
        return false;
    }
    return true;
}

bool Hanoi::estadoIgual(Hanoi h){
    if(pinos[0].size() != h.pinos[0].size()){
        return false;
    }else if(pinos[1].size() != h.pinos[1].size()){
        return false;
    }else if(pinos[2].size() != h.pinos[2].size()){
        return false;
    }

    for(int i=0;i<pinos[0].size();i++){
        if(pinos[0][i] != h.pinos[0][i]){
            return false;
        }
    }
    for(int i=0;i<pinos[1].size();i++){
        if(pinos[1][i] != h.pinos[1][i]){
            return false;
        }
    }
    for(int i=0;i<pinos[2].size();i++){
        if(pinos[2][i] != h.pinos[2][i]){
            return false;
        }
    }

    return true;
}
