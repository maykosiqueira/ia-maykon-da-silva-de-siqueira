#include <iostream>

#include "buscaLargura.h"
#include "buscaProfundidade.h"
#include "buscaCustoUniforme.h"
#include "Hanoi.h"
using namespace std;

int main()
{
    int discos;
    int opt;
    cout << "Digite a quantidade de discos: ";
    cin >> discos;
    do{
        cout << "Escolha o algoritmo de busca" << endl;
        cout << "   1 - Busca em largura" << endl;
        cout << "   2 - Busca em profundidade" << endl;
        cout << "   3 - Busca de custo uniforme" << endl;
        cout << "Opcao: ";
        cin >> opt;
    }while(opt < 1 || opt > 3);


    Hanoi h(discos);
    BuscaLargura largura(h);
    BuscaProfundidade profundidade(h);
    BuscaCustoUniforme uniforme(h);
    switch(opt){
        case 1:
            largura.processar();
            break;
        case 2:
            profundidade.processar();
            break;
        case 3:
            uniforme.processar();
            break;
    }

    return 0;
}
