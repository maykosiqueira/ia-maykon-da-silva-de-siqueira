#include "buscaLargura.h"

BuscaLargura::BuscaLargura(Hanoi h) {
    estados.push_back(h);
    contador = 0;
}

//Realiza o processamento da torre de hanoi
void BuscaLargura::processar() {
    vector<Hanoi> todos;
    vector<int> resultado;
    Hanoi tmp;
    int percorridos=0;

    processados.push_back(estados.front());
    todos.push_back(estados.front());

    while(true) {
        tmp = estados.front();
        if(tmp.fimAlcancado()) { //Verifica se alcancou o fim
            todos.push_back(tmp);
            resultado.push_back(todos.size()-1);

            //Pega as ordens dos pais
            while(tmp.getPai() != 0){
                resultado.push_back(tmp.getPai());
                tmp = todos[tmp.getPai()];
            }

            //Inverte para sair corretamene
            reverse(resultado.begin(), resultado.end());
            percorridos = resultado.size();
            for(int i=0;i<percorridos;i++){
                todos[resultado[i]].exibePinos();
            }

            //Percorridos-1 devido a quantidade de nohs percorridos ignorar o inicial
            cout << endl << endl << "Quantidade de nohs percorridos: " << percorridos-1 << endl;
            cout << "Quantidade de nohs conferidos: " << contador << endl;
            return;
        } else {
            contador++;
            if(tmp.mover(0,1)) {
                if(!jahProcessado(tmp)) {
                    //Seta o pai
                    tmp.setPai(contador);
                    //Vetor com estados processados
                    processados.push_back(tmp);
                    //Como acabou de expandir, coloca para processar
                    estados.push_back(tmp);
                }
            }
            tmp = estados.front();
            if(tmp.mover(0,2) ) {
                if(!jahProcessado(tmp)) {
                    //Seta o pai
                    tmp.setPai(contador);
                    //Vetor com estados processados
                    processados.push_back(tmp);
                    //Como acabou de expandir, coloca para processar
                    estados.push_back(tmp);
                }
            }

            tmp = estados.front();
            if(tmp.mover(1,2)) {
                if(!jahProcessado(tmp)) {
                    //Seta o pai
                    tmp.setPai(contador);
                    //Vetor com estados processados
                    processados.push_back(tmp);
                    //Como acabou de expandir, coloca para processar
                    estados.push_back(tmp);
                }
            }

            tmp = estados.front();
            if(tmp.mover(2,0)) {
                if(!jahProcessado(tmp)) {
                    //Seta o pai
                    tmp.setPai(contador);
                    //Vetor com estados processados
                    processados.push_back(tmp);
                    //Como acabou de expandir, coloca para processar
                    estados.push_back(tmp);
                }
            }

            tmp = estados.front();
            if(tmp.mover(1,0)) {
                if(!jahProcessado(tmp)) {
                    //Seta o pai
                    tmp.setPai(contador);
                    //Vetor com estados processados
                    processados.push_back(tmp);
                    //Como acabou de expandir, coloca para processar
                    estados.push_back(tmp);
                }
            }

            tmp = estados.front();
            if(tmp.mover(2,1) ) {
                if(!jahProcessado(tmp)) {
                    //Seta o pai
                    tmp.setPai(contador);
                    //Vetor com estados processados
                    processados.push_back(tmp);
                    //Como acabou de expandir, coloca para processar
                    estados.push_back(tmp);
                }
            }
            todos.push_back(estados.front());
            estados.pop_front();

        }
    }
}
bool BuscaLargura::jahProcessado(Hanoi h) {
    for(int i=0; i<processados.size(); i++) {
        if(h.estadoIgual(processados[i]))
            return true;
    }
    return false;
}
