#ifndef _HANOI_H_
#define _HANOI_H_

#include <iostream>
#include <iomanip>
#include <vector>


using namespace std;

class Hanoi{
public:
    Hanoi(int disco);
    Hanoi();
    bool mover(int pinoOrigem, int pinoDestino);
    void exibePinos();
    bool estadoIgual(Hanoi h);
    bool fimAlcancado();
    int getPai(){
        return pai;
    };
    void setPai(int pos){
        pai = pos;
    };


     bool operator<( const Hanoi& val ) const {
    	return pai < val.pai;
    }
private:
    vector<vector<int>> pinos;

    string centerOut(int width, const string& str) {
        int len = str.length();
        if(width < len) { return str; }

        int diff = width - len;
        int pad1 = diff/2;
        int pad2 = diff - pad1;
        return string(pad1, ' ') + str + string(pad2, ' ');
    }

    int pai;
};

#endif // _HANOI_H_
