#ifndef _CUSTOUNIFORME_H_
#define _CUSTOUNIFORME_H_

#include <list>
#include <algorithm>
#include <vector>

#include "Hanoi.h"

class BuscaCustoUniforme{
public:
    BuscaCustoUniforme(Hanoi h);
    void processar();
private:
    list<Hanoi> estados;
    vector<Hanoi> processados;
    int contador;
    bool jahProcessado(Hanoi h);

};

#endif // _CUSTOUNIFORME_H_
