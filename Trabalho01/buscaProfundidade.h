#ifndef _PROFUNDIDADE_H_
#define _PROFUNDIDADE_H_

#include <list>
#include <vector>
#include <algorithm>

#include "Hanoi.h"

class BuscaProfundidade{
public:
    BuscaProfundidade(Hanoi h);
    void processar();
private:
    list<Hanoi> estados;
    vector<Hanoi> processados;
    int contador;
    bool jahProcessado(Hanoi h);
};

#endif // _PROFUNDIDADE_H_
