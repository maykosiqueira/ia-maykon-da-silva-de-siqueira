#ifndef _BUSCALARGURA_H_
#define _BUSCALARGURA_H_

#include <list>
#include <algorithm>
#include <vector>

#include "Hanoi.h"

class BuscaLargura{
public:
    BuscaLargura(Hanoi h);
    void processar();
private:
    list<Hanoi> estados;
    vector<Hanoi> processados;
    int contador;
    bool jahProcessado(Hanoi h);

};

#endif // _BUSCALARGURA_H_

