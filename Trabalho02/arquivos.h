#ifndef _ARQUIVOS_H_
#define _ARQUIVOS_H_

#include <iostream>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

class Arquivos{
public:
    Arquivos(string path){
        //Seta o path
        _path = path;
    };
    void readData(char **matriz);
    vector<int> readMatrixSize();

private:
    string _path;
    ifstream file;
    int _maxX;
    int _maxY;
};

#endif // _ARQUIVOS_H_
