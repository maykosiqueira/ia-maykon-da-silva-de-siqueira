#include "arquivos.h"
void Arquivos::readData(char **matriz){
    //Variaveis para leitura dos dados
    int fileLineRead = 0;
    char tmp[256]={'\0'};
    int x=0;
    int y=0;

     //Abre o arquivo e testa a abertura
    file.open(_path.c_str());
    if(!file){//Se ocorrer um erro sobe excessao
        throw "Falha ao carregar o arquivo!";
    }

    //Vai para o inicio do arquivo
    file.clear();
    file.seekg(0, ios::beg);


    //Realiza a leitura do arquivo
    while(!file.eof()){

        //Checa se é a primeira linha, se for ignora
        if(!fileLineRead ){
            //Pega uma linha e joga num buffer temporario para pular a primeira linha
            file.getline(tmp,256);
            fileLineRead++;
            continue;
        }

        //Pega uma linha e joga num buffer temporario
        file.getline(tmp,256);

        //Se for fim de linha sai do laço
        if(file.eof())
            break;


        int i;

        if(x>=_maxX){
            throw "Arquivo possui mais linhas que o especificado";
        }

        for(i=0;i<256;i++){//Le a origem
            //Se acabou a linha vai para a proxima
            if(tmp[i] == '\n' || tmp[i] == '\0'){
                if(y<_maxY){
                    throw "Arquivo possui menos colunas que o especificado";
                }
                y=0;
                x++;
                break;
            }

            if(y>=_maxY){
                throw "Arquivo possui mais colunas que o especificado";
            }
            y++;
            //Se encontrar um espaco, entrada ou saida, Adiciona a matriz
            if(tmp[i] == ' ' || tmp[i] == 'E' || tmp[i] == 'S'){
                matriz[x][i] = tmp[i];
            }else{//Senao adiciona como parede
                matriz[x][i] = '#';
            }

        }

    }
    if(x<_maxX){
        throw "Arquivo possui menos linhas que o especificado";
    }
}
vector<int> Arquivos::readMatrixSize(){
    string ySize;
    string xSize;
    vector<int> ret;
    char tmp[256]={'\0'};

    bool yFinish = false;
    bool yStart = false;

    //Abre o arquivo e testa a abertura
    file.open(_path.c_str());
    if(!file){//Se ocorrer um erro sobe excessao
        throw "Falha ao carregar o arquivo!";
    }

    if(!file.eof()){
        //Pega uma linha e joga num buffer temporario
        file.getline(tmp,256);
    }else{
        throw "Arquivo vazio!";
    }


    //A primeira linha do arquivo deve ser somente numeros,ela represena o tamanho total da mariz que sera lida
    for(int i=0;i<256;i++){
            //Se acabou a linha sai do loop
            if(tmp[i] == '\n' || tmp[i] == '\0'){
                file.close();
                break;
            }
            if(tmp[i] == ' ' && !yFinish && yStart){//Detecta se acabou Y
                yFinish = true;
            }else if(isdigit(tmp[i])){//Verifica se eh um numero
                if(!yFinish){
                    if(!yStart)//Faz isso para poder ignorar espacos em branco no inicio
                        yStart = true;

                    ySize += tmp[i];
                }else{
                    xSize += tmp[i];
                }

            }else if(tmp[i] != ' '){
                throw "Formato invalido.";
            }
    }

    ret.push_back(stoi(ySize));
    ret.push_back(stoi(xSize));

    //Armazena o X e Y lidos para usar na carga dos dados
    _maxY = ret[0];
    _maxX = ret[1];

    return ret;
}

