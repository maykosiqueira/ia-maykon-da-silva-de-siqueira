#include "problema.h"
Problema::Problema(string arquivo){
    Arquivos arq(arquivo);
    vector<int> matrixDimensions;
    pai = 0;
    passos = 0;
    percorridos = 0;
    try{
        matrixDimensions = arq.readMatrixSize();
        //Pega o tamanho maximo da matriz
        _maxY = matrixDimensions[0];
        _maxX = matrixDimensions[1];

        //Aloca a matriz do labirinto
        matriz = new char*[_maxX];
        for(int i=0;i<_maxX;i++){
            matriz[i] = new char[_maxY];
        }
        //Carrega o txt
        arq.readData(matriz);


    }catch(const char *err){
        cout << err << endl;
        exit(-1);
    }
}

Problema::~Problema(){
    //Desaloca a matriz dos dados
    for(int i=0;i<_maxX;i++){
        delete[] matriz[i];
    }

    delete []matriz;
}
//Printa o labirinto
void Problema::exibeLabirinto(bool resul){
    for(int x=0;x<_maxX;x++){
        for(int y=0;y<_maxY;y++){
            cout << matriz[x][y];
        }
        cout << endl;
    }
    cout << endl;
    if(resul){
        cout << "Encontrado em: " << xFound +1 << ";" << yFound +1 << endl;
        cout << "Passos para encontrar: " << passos << endl;
        cout << "Passos percorridos: " << percorridos << endl;
    }else{
        cout << "Infelizmente a busca não localizou uma saida." << endl;
    }
}
bool Problema::buscaGulosa(){
    for(int x=0;x<_maxX;x++){//Localiza a saida
        for(int y=0;y<_maxY;y++){
            if(matriz[x][y] == 'S'){
               saida.x = x;
               saida.y = y;
               saida.custo = 0;
            }
        }
    }
    for(int x=0;x<_maxX;x++){//Localiza a entrada
        for(int y=0;y<_maxY;y++){
            if(matriz[x][y] == 'E'){
               return buscaGulosa(x,y,0);
            }
        }
    }

    return false;
}

bool Problema::buscaAStar(){
    for(int x=0;x<_maxX;x++){//Localiza a saida
        for(int y=0;y<_maxY;y++){
            if(matriz[x][y] == 'S'){
               saida.x = x;
               saida.y = y;
               saida.custo = 0;
            }
        }
    }
    for(int x=0;x<_maxX;x++){//Localiza a entrada
        for(int y=0;y<_maxY;y++){
            if(matriz[x][y] == 'E'){
               return buscaAStar(x,y,0,0,0);
            }
        }
    }

    return false;
}
/*Implementação do algoritmo de busca Gulosa*/
bool Problema::buscaGulosa(int x, int y, int custo){
    int xTmp;
    int yTmp;
    int custoTmp;
    noh nohTmp(x,y,custo);

    vector<Distancias> distancias;//Distancia inicial eh infinita
    distancias.push_back(Distancias(9999999,1));
    distancias.push_back(Distancias(9999999,2));
    distancias.push_back(Distancias(9999999,3));
    distancias.push_back(Distancias(9999999,4));

    //Checa se achou o fim
    if(matriz[x][y] == 'S' ||
        (y-1 >=0 && matriz[x][y-1] == 'S') ||
        (y+1 < _maxY && matriz[x][y+1] == 'S') ||
        (x-1 >=0 && matriz[x-1][y] == 'S') ||
        (x+1 < _maxX && matriz[x+1][y] == 'S')
        ){
        xFound = x;
        yFound = y;
        matriz[x][y] = '1';
        return true;
    }
    //Se nao achou o fim, vai precisar de +1 passo
    passos ++;


    if(y-1 >=0 && !visitou(noh(x,y-1)) && matriz[x][y-1] == ' '){//Ve se o elemeno a esquerda eh vazio, se for, adiciona ele
        distancias[0].distancia = distanciaEuclidiana(x,y-1);
    }
    if(y+1 < _maxY && !visitou(noh(x,y+1)) && matriz[x][y+1] == ' '){//Ve se o elemeno a direita eh vazio, se for, adiciona ele
        distancias[1].distancia = distanciaEuclidiana(x,y+1);
    }
    if(x-1 >=0 && !visitou(noh(x-1,y)) && matriz[x-1][y] == ' '){//Ve se o elemeno a acima eh vazio, se for, adiciona ele
        distancias[2].distancia = distanciaEuclidiana(x-1,y);
    }
    if(x+1 < _maxX && !visitou(noh(x+1,y)) && matriz[x+1][y] == ' '){//Ve se o elemeno a abaixo eh vazio, se for, adiciona ele
        distancias[3].distancia= distanciaEuclidiana(x+1,y);
    }

    //Pega a menor distancia
    sort(distancias.begin(),distancias.end(),menorDisancia);

    //Pega a menor distancia euclidiana para ir para o proximo
    if(distancias[0].distancia < 9999999){//Se a distancia for menor que infinito
        switch(distancias[0].id){
            case 1:
                nohs.push_back(noh(x,y-1, distanciaEuclidiana(x,y-1)));
                break;
            case 2:
                nohs.push_back(noh(x,y+1,  distanciaEuclidiana(x,y+1)));
                break;
            case 3:
                nohs.push_back(noh(x-1,y,  distanciaEuclidiana(x-1,y)));
                break;
            case 4:
                nohs.push_back(noh(x+1,y,  distanciaEuclidiana(x+1,y)));
                break;
        }
    }

    visitados.push_back(nohTmp);

    do{
        if(nohs.size() == 0)
            return false;
        xTmp = nohs.back().x;
        yTmp = nohs.back().y;
        custoTmp = nohs.back().custo;
        nohs.pop_back();
    }while(!buscaGulosa(xTmp,yTmp, custoTmp));

    return false;
}

/*Implementação do algoritmo de busca A* (Estrela)*/
bool Problema::buscaAStar(int x, int y, int custo, int npassos, int npai){
    int xTmp;
    int yTmp;
    int custoTmp;
    int passosTmp;
    int paiTmp;
    noh nohTmp(x,y,custo, npassos,npai);

    vector<Distancias> distancias;//Distancia inicial eh infinita
    distancias.push_back(Distancias(9999999,1));
    distancias.push_back(Distancias(9999999,2));
    distancias.push_back(Distancias(9999999,3));
    distancias.push_back(Distancias(9999999,4));

    //Checa se achou o fim
    if(matriz[x][y] == 'S' ||
        (y-1 >=0 && matriz[x][y-1] == 'S') ||
        (y+1 < _maxY && matriz[x][y+1] == 'S') ||
        (x-1 >=0 && matriz[x-1][y] == 'S') ||
        (x+1 < _maxX && matriz[x+1][y] == 'S')
        ){
        xFound = x;
        yFound = y;
        matriz[x][y] = '1';
        return true;
    }
    //Usado para exibir o caminho
    pai ++;

    //Se nao achou o fim, vai precisar de +1 passo
    npassos ++;
    passos ++;

    if(y-1 >=0 && !visitou(noh(x,y-1)) && matriz[x][y-1] == ' '){//Ve se o elemeno a esquerda eh vazio, se for, adiciona ele
        distancias[0].distancia = npassos + distanciaEuclidiana(x,y-1);
    }
    if(y+1 < _maxY && !visitou(noh(x,y+1)) && matriz[x][y+1] == ' '){//Ve se o elemeno a direita eh vazio, se for, adiciona ele
        distancias[1].distancia = npassos + distanciaEuclidiana(x,y+1);
    }
    if(x-1 >=0 && !visitou(noh(x-1,y)) && matriz[x-1][y] == ' '){//Ve se o elemeno a acima eh vazio, se for, adiciona ele
        distancias[2].distancia = npassos + distanciaEuclidiana(x-1,y);
    }
    if(x+1 < _maxX && !visitou(noh(x+1,y)) && matriz[x+1][y] == ' '){//Ve se o elemeno a abaixo eh vazio, se for, adiciona ele
        distancias[3].distancia= npassos + distanciaEuclidiana(x+1,y);
    }

    if(distancias[0].distancia < 9999999){//Se a distancia for menor que infinito
        nohs.push_back(noh(x,y-1,distancias[0].distancia,npassos,pai));
    }
    if(distancias[1].distancia < 9999999){//Se a distancia for menor que infinito
        nohs.push_back(noh(x,y+1,distancias[1].distancia,npassos,pai));
    }
    if(distancias[2].distancia < 9999999){//Se a distancia for menor que infinito
        nohs.push_back(noh(x-1,y, distancias[2].distancia,npassos,pai));
    }
    if(distancias[3].distancia < 9999999){//Se a distancia for menor que infinito
        nohs.push_back(noh(x+1,y, distancias[3].distancia,npassos,pai));
    }


    visitados.push_back(nohTmp);

    //Ordena o vetor para fazer um custo uniforme
    sort(nohs.begin(),nohs.end(),menorNoh);

    do{
        if(nohs.size() == 0)
            return false;
        xTmp = nohs.back().x;
        yTmp = nohs.back().y;
        custoTmp = nohs.back().custo;
        passosTmp = nohs.back().passos;
        paiTmp = nohs.back().pai;
        nohs.pop_back();
    }while(!buscaAStar(xTmp,yTmp, custoTmp, passosTmp, paiTmp));

    return true;
}

//Checa se a posicao foi visitada
bool Problema::visitou(noh v){
    int tam = visitados.size();
    for(int i=0;i<tam;i++){
        if(visitados[i].x == v.x && visitados[i].y == v.y){
            return true;
        }
    }
    return false;
}

void Problema::processaGulosa(){
    int i=visitados.size()-1;
    while(i != 0){
        matriz[visitados[i].x][visitados[i].y] = '1';
        i--;
        percorridos ++;
    }
}
void Problema::processaAStar(){
    int i=visitados.size()-1;
    while(i != 0){
        matriz[visitados[i].x][visitados[i].y] = '1';
        i = visitados[i].pai - 1;
        percorridos ++;
    }
}
int Problema::distanciaEuclidiana(noh novo){
    return sqrt(((saida.x - novo.x) * (saida.x - novo.x)) + ((saida.y - novo.y) * (saida.y - novo.y)));
}
int Problema::distanciaEuclidiana(int x, int y){
    return sqrt(((saida.x - x) * (saida.x - x)) + ((saida.y - y) * (saida.y - y)));
}
bool Problema::menorDisancia(Distancias d1,Distancias d2){
    return d1.distancia < d2.distancia;
}
bool Problema::menorNoh(noh n1, noh n2){
    return n1.custo < n2.custo;
}

