#ifndef _PROBLEMA_H_
#define _PROBLEMA_H_

#include <vector>
#include <algorithm>
#include "arquivos.h"

class Distancias{
public:
    Distancias(int dis,int idd){
        distancia = dis;
        id = idd;
    }

    int distancia;
    int id;

};

class noh{
public:
    noh(){
        x = 0;
        y = 0;
        custo = 0;
        passos = 0;
    }
    noh(int nX,int nY){
        x = nX;
        y = nY;
        custo = 0;
        passos = 0;
    };
    noh(int nX,int nY, int nCusto){
        x = nX;
        y = nY;
        custo = nCusto;
        passos = 0;
    };
    noh(int nX,int nY, int nCusto, int nPassos, int nPai){
        x = nX;
        y = nY;
        custo = nCusto;
        passos = nPassos;
        pai = nPai;
    };

    int x;
    int y;
    int custo;
    int passos;
    int pai;
};

class Problema{
public:
    Problema(string arquivo);
    ~Problema();
    bool buscaGulosa();
    bool buscaGulosa(int x, int y, int custo);
    void exibeLabirinto(bool resul = false);
    void processaGulosa();


    bool buscaAStar();
    bool buscaAStar(int x, int y, int custo, int npassos, int npai);
    void processaAStar();


private:
    char **matriz;
    int _maxX;
    int _maxY;
    int pai;
    int passos;
    int percorridos;
    int yFound;
    int xFound;
    vector<noh> nohs;
    vector<noh> visitados;

    int distanciaEuclidiana(noh novo);
    int distanciaEuclidiana(int x, int y);

    noh saida;

    bool visitou(noh v);
    static bool menorNoh(noh n1, noh n2);
    static bool menorDisancia(Distancias d1,Distancias d2);
};

#endif // _PROBLEMA_H_
