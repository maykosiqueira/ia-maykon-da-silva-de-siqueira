#include <iostream>

#include "problema.h"

using namespace std;

int main()
{
    string arquivo;
    int opt;
    cout << "***** MAZE SOLVER ******" << endl << endl;
    cout << "Digite o nome do arquivo que deseja carregar: ";
    cin >> arquivo;
    Problema p(arquivo);

    do{
        cout << "1 - Busca Gulosa" << endl;
        cout << "2 - Busca A*" << endl;
        cout << "Digite a busca desejada:" << endl;
        cin >> opt;
    }while(opt < 1 || opt > 2);

    switch(opt){
        case 1:
            if(p.buscaGulosa()){
                p.processaGulosa();
                p.exibeLabirinto(true);
            }else{
                p.processaGulosa();
                p.exibeLabirinto(false);
            }
            break;
        case 2:
            if(p.buscaAStar()){
                p.processaAStar();
                p.exibeLabirinto(true);
            }else{
                p.processaAStar();
                p.exibeLabirinto(false);
            }
            break;
    }


    return 0;
}
