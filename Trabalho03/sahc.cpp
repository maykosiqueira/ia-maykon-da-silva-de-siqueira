#include "sahc.h"

void SAHC::processa(){
    vector<int> melhorSolucao;
    vector<vector<int>>  solucoes;//N Solucoes geradas
    int repeticoes = 10;
    bool trocou = false;

    int ite=0;
    int cong=0;

    melhorSolucao = prob->sucessor();

    while(ite < iteracoesMaxima && cong < congelamentoMaximo){
        for(int i=0;i<repeticoes;i++){//Sucessores da funcao inicial
            solucoes.push_back(prob->sucessor());
        }

        for(int i=0;i<repeticoes;i++){
            if(prob->avaliacao(melhorSolucao.back(),solucoes[i].back())){//Ve se uma solucao eh melhor que a melhor, se for troca
                melhorSolucao = solucoes[i];
                trocou = true;
            }
        }

        solucoes.erase(solucoes.begin(),solucoes.end());
        if(trocou){
            prob->setSolucao(melhorSolucao);
            cong = 0;
            trocou = false;
        }else{
            cong ++;
        }

    }
    cout << endl << "***Solucao Final***" << endl;
    prob->setSolucao(melhorSolucao);
    prob->exibeSolucao();
}
