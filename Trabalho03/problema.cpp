#include "problema.h"

Problema::Problema(string file){
    custo = 0;
    Arquivos arq(file);
    try{
        //Le o tamanho da matriz
        _matrizSize = arq.readMatrixSize();
        _useMatrizSize = _matrizSize-1;
        if(_matrizSize < 3){
            cout << "A matriz deve conter pelo menos 3 cidades." << endl;
            exit(-2);
        }

        //Aloca espaco para a solucao
        _solucao = new int[_matrizSize];

        //Aloca as linhas
        _matrix = new int*[_matrizSize];

        //Aloca as colunas
        for(int i=0;i<_matrizSize;i++){
            _matrix[i] = new int[_matrizSize];
        }

        //Carrega os dados do arquivo para a matriz
        arq.readData(_matrix);

        geraSolucaoInicial();
    }catch (const char* err){
        cout << err << endl;
        exit(-1);
    }
}


//Gera a solucao inicial
void Problema::geraSolucaoInicial(){
    int random;
    int numero = 0;
    float percentagem = 0.8;//Porcentagem do grasp
    int graspPercurse = _matrizSize * percentagem;//Quantidade
    vector<int> posicoesRestantes;

    for(int i=0;i<_matrizSize;i++){//Armazena as possiveis posicoes
        posicoesRestantes.push_back(i);
    }

    //Inicializa o rand
    srand(time(NULL));



    int i=0;//Variavel usada para contar quais posicoes ja foram preenchidas
    grasp(&i,graspPercurse,&posicoesRestantes);//Preenche o inicio usando grasp

    //Prenche os caminhos aleatoriamente
    for(;i<_matrizSize;i++){
        random = rand() % posicoesRestantes.size();
        numero = posicoesRestantes[random];//Gera uma nova posicao aleatoria
        posicoesRestantes.erase(posicoesRestantes.begin() + random);//Remove o numero do vetor
        _solucao[i] = numero;//Adiciona o numero na posicao

    }
    custo = recalculaCusto(_solucao);
    cout << "***Solucao Inicial***" << endl;
    exibeSolucao();

}
vector<int> Problema::sucessor(){
    int pos1;
    int pos2;
    int posTmp;
    int *solucaoTmp;
    int custoTmp;
    vector<int> solucao;

    //Solucao temporaria
    solucaoTmp = new int[_matrizSize];

    //Copia a solucao atual
    for(int i=0;i<_matrizSize;i++){
        solucaoTmp[i] = _solucao[i];
    }

    //Pega a primeira posicao de troca aleatoria
    pos1 = rand() % _matrizSize;

    do{
        //Pega a segunda posicao de troca aleatoria
        pos2 = rand() % _matrizSize;
    }while(pos2 == pos1);//Checa se as posicoes geradas sao iguais, se forem gera novas

    //Troca as cidades de posicao
    posTmp = solucaoTmp[pos1];
    solucaoTmp[pos1] = solucaoTmp[pos2];
    solucaoTmp[pos2] = posTmp;

    //Joga pro vetor as novas posicoes
    for(int i=0;i<_matrizSize;i++){
        solucao.push_back(solucaoTmp[i]);
    }

    custoTmp = recalculaCusto(solucaoTmp);
    //Guarda também o custo
    solucao.push_back(custoTmp);

    return solucao;

}
void Problema::grasp(int *iterador,int maxSize, vector<int> *posicoes){
    int qtdePontos = _matrizSize*_matrizSize;
    int menor = 9999999999;//INF
    int xMenor=0,yMenor=0;
    int posicao = 0;
    int x,y;
    for(int i=0;i<qtdePontos;i++){//Varre toda a matriz procurando o menor valor
        x = i/_matrizSize;
        y = i%_matrizSize;
        //Checa se encontrou o menor e pega como o menor valor
        if(_matrix[x][y] < menor){
            menor = _matrix[x][y];
            xMenor = x;
            yMenor=y;
        }
    }

    removeVector(posicoes,xMenor);
    removeVector(posicoes,yMenor);
    _solucao[0] = xMenor;
    _solucao[1] = yMenor;

    for(*iterador=2;*iterador<=maxSize;(*iterador)++){
        menor = 9999999999;
        x = _solucao[*iterador-1];//X é o valor anterior (origem)
        for(int i=0;i<_matrizSize;i++){
            if(_matrix[x][i] < menor && isValid(posicoes,i)){
                menor = _matrix[x][i];
                posicao = i;//Salva a posicao que achou o menor valor
            }
        }
        removeVector(posicoes,posicao);
        _solucao[*iterador] = posicao;//Armazena a posicao encontrada
    }

}
bool Problema::isValid(vector<int> *posicoes, int posicao){
    for(int i=0;i<posicoes->size();i++){
        if(posicoes->at(i) == posicao)
            return true;
    }
    return false;
}
void Problema::removeVector(vector<int> *posicoes,int posicao){
    for(int i=0;i<posicoes->size();i++){
        if(posicoes->at(i) == posicao)
        posicoes->erase(posicoes->begin() + i);//Remove o numero do vetor
    }
}

int Problema::recalculaCusto(int *solucao){
    int custoTmp = 0;
    for(int i=1;i<_matrizSize;i++){//Pega as origens e destinos e pega os seus respectivos custos
        custoTmp += _matrix[solucao[i-1]][solucao[i]];
    }
    custoTmp += _matrix[solucao[_matrizSize-1]][solucao[0]];
    return custoTmp;
}

bool Problema::avaliacao(int solucaoAtual, int novaSolucao){
    return solucaoAtual > novaSolucao;
}
void Problema::setSolucao(vector<int> s){
    //Pega o custo
    custo = s.back();
    s.pop_back();

    for(int i=0;i<_matrizSize;i++){
        _solucao[i] = s[i];
    }
}
