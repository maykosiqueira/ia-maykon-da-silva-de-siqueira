#ifndef _PROBLEMA_H_
#define _PROBLEMA_H_

#include <vector>
#include <algorithm>
#include <time.h>
#include "arquivos.h"

using  namespace std;

class Problema{
public:
    Problema(string file);
    ~Problema(){
        //Apaga as colunas da matriz
        for(int i=0;i<_matrizSize;i++){
            delete [] _matrix[i];
        }
        delete [] _matrix;

        //Apaga a solucao
        delete [] _solucao;
    }

    vector<int> sucessor();
    bool avaliacao(int solucaoAtual,int novaSolucao);
    void exibeSolucao(){
        cout << "Caminho: ";
        for(int i=0;i<_matrizSize;i++){
            cout << _solucao[i] << " ";
        }
        cout << endl << "Custo: " << custo << endl;
    };

    void setSolucao(vector<int> s);



private:
    int _matrizSize;
    int _useMatrizSize;
    int **_matrix;
    int *_solucao;
    int *novasolucao;
    int custo;

    bool isValid(vector<int> *posicoes, int posicao);
    void geraSolucaoInicial();
    void grasp(int *iterador, int maxSize, vector<int> *posicoes);
    void removeVector(vector<int> *posicoes,int posicao);
    int recalculaCusto(int *solucao);
};

#endif // _PROBLEMA_H_

