#include <iostream>

#include "sahc.h"
#include "problema.h"


using namespace std;

int main()
{
    string arqCaminhos;
    cout << "Digite o arquivo que deseja carregar: ";
    cin >> arqCaminhos;


    Problema p(arqCaminhos);
    SAHC sahc(&p,1000000,100000);

    sahc.processa();

    return 0;
}
