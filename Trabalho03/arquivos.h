#ifndef _ARQUIVOS_H_
#define _ARQUIVOS_H_

#include <iostream>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

class Arquivos{
public:
    Arquivos(string path){
        //Seta o path
        _path = path;
        maiorVertice = 0;
    };
    void readData(int **matriz);
    int readMatrixSize();

private:
    string _path;
    ifstream file;
    int maiorVertice;
};

#endif // _ARQUIVOS_H_
