#include "arquivos.h"
void Arquivos::readData(int **matriz){
    //Variaveis para leitura dos dados
    int x, y, peso;
    int contador = 0;
    int fileLineRead = 0;
    char tmp[256]={'\0'};
    string pes;
    bool finishNumber = false;

    int linhasColunas=readMatrixSize();
    int qtdePontos = linhasColunas*linhasColunas;

        //Abre o arquivo e testa a abertura
    file.open(_path.c_str());
    if(!file){//Se ocorrer um erro sobe excessao
        throw "Falha ao carregar o arquivo!";
    }

    //Vai para o inicio do arquivo
    file.clear();
    file.seekg(0, ios::beg);


    //Realiza a leitura do arquivo
    while(!file.eof()){

        //Checa se é a primeira linha, se for ignora
        if(!fileLineRead ){
            //Pega uma linha e joga num buffer temporario para pular a primeira linha
            file.getline(tmp,256);
            fileLineRead++;
            continue;
        }
        //Limpa as variaveis
        pes = "";

        //Pega uma linha e joga num buffer temporario
        file.getline(tmp,256);

        //Se for fim de linha sai do laço
        if(file.eof())
            break;


        int i;
        for(i=0;i<256;i++){//Le a origem
            //Se acabou a linha vai para a proxima
            if(tmp[i] == '\n' || tmp[i] == '\0'){
                if(!finishNumber){
                    if(contador >= qtdePontos){
                        throw "O arquivo possui mais dados do que o informado";
                    }
                    //Converte de texto para int
                    x= contador / linhasColunas;//Linhas
                    y= contador % linhasColunas;//Colunas
                    peso = stoi(pes);
                    //Converte o valor encontrado para a matriz
                    matriz[x][y] = peso;

                    //Limpa as variaveis
                    pes = "";
                    //Indica que terminou o numero
                    finishNumber = true;
                    contador ++;//Vai para o proximo numero
                }
                break;
            }
            //Se encontrar um espaco, vai para o proximo caracter
            if(tmp[i] == ' '){
                if(!finishNumber && pes != ""){
                    if(contador >= qtdePontos){
                        throw "O arquivo possui mais dados do que o informado";
                    }
                    //Converte de texto para int
                    x= contador / linhasColunas;//Linhas
                    y= contador % linhasColunas;//Colunas
                    peso = stoi(pes);
                    //Converte o valor encontrado para a matriz
                    matriz[x][y] = peso;

                    //Limpa as variaveis
                    pes = "";
                    //Indica que terminou o numero
                    finishNumber = true;
                    contador ++;//Vai para o proximo numero
                }
                continue;
            }
            //Pega o numero para conversao
            if(isdigit(tmp[i])){
                pes += tmp[i];
                if(finishNumber)//Se achou um numero, continua
                    finishNumber = false;
            }else{//Nao pode achar letras
                throw "Caracteres nao permitidos encontrados, abortando...";
            }
        }

    }


    if(contador < qtdePontos){
        throw "O arquivo possui menos dados do que o informado";
    }

}
int Arquivos::readMatrixSize(){
    string matrixSize;
    int sizeConvert;
    char tmp[256]={'\0'};

            //Abre o arquivo e testa a abertura
    file.open(_path.c_str());
    if(!file){//Se ocorrer um erro sobe excessao
        throw "Falha ao carregar o arquivo!";
    }

    if(!file.eof()){
        //Pega uma linha e joga num buffer temporario
        file.getline(tmp,256);
    }else{
        throw "Arquivo vazio!";
    }


    //A primeira linha do arquivo deve ser somente numeros,ela represena o tamanho total da mariz que sera lida
    for(int i=0;i<256;i++){
            //Se acabou a linha sai do loop
            if(tmp[i] == '\n' || tmp[i] == '\0'){
                file.close();
                break;
            }
            if(isdigit(tmp[i])){//Verifica se eh um numero
                matrixSize += tmp[i];
            }else{
                throw "Formato invalido.";
            }
    }

    sizeConvert = stoi(matrixSize);

    return sizeConvert;
}

