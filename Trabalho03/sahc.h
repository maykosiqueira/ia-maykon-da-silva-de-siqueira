#ifndef _SAHC_H_
#define _SAHC_H_

#include "problema.h"

/*Steepest Ascent Hill Climbing*/
class SAHC{
public:
    SAHC(Problema *p,int maxIteracoes,int maxCongelamento){
        prob = p;
        congelamentoMaximo = maxCongelamento;
        iteracoesMaxima = maxIteracoes;
    }

    void processa();

private:
    Problema *prob;
    int iteracoesMaxima;
    int congelamentoMaximo;

};

#endif
